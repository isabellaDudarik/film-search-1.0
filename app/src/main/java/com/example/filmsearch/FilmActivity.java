package com.example.filmsearch;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilmActivity extends AppCompatActivity {
    @BindView(R.id.nameFilm)
    TextView nameFilm;

    @BindView(R.id.descriptionFilm)
    TextView descriptionFilm;

    @BindView(R.id.photoUrl)
    ImageView photoUrl;

    @BindView(R.id.vouteCount)
    TextView vouteCount;

    @BindView(R.id.popularity)
    TextView popularity;

    @BindView(R.id.isAdult)
    TextView isAdult;

    @BindView(R.id.year)
    TextView year;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);

        ButterKnife.bind(this);

        String name = getIntent().getStringExtra(MainActivity.EXTRA_TITLE);
        nameFilm.setText(name);
        descriptionFilm.setText(getIntent().getStringExtra(MainActivity.EXTRA_DESCR));
        Glide.with(this).load(getIntent().getStringExtra(MainActivity.EXTRA_PICTURE)).into(photoUrl);
        popularity.setText(getIntent().getStringExtra(MainActivity.EXTRA_POPULARITY));
        year.setText(getIntent().getStringExtra(MainActivity.EXTRA_YEAR));


        if (getIntent().getBooleanExtra(MainActivity.EXTRA_ADULT, true)) {
            isAdult.setVisibility(View.VISIBLE);
        } else {
            isAdult.setVisibility(View.GONE);
        }

        vouteCount.setVisibility(View.GONE);



    }
}
