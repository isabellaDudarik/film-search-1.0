package com.example.filmsearch.utils;

import com.example.filmsearch.model.FilmsModel;
import com.example.filmsearch.model.ResponseModel;


import java.util.ArrayList;
import java.util.List;

public class Converter {
    public static List<FilmsModel> convertRequest(ResponseModel responseModel, int idForItem) {

        List<FilmsModel> films = new ArrayList<>();

        for (int i = 0; i < responseModel.getResults().size(); i++) {
            ResponseModel.Result filmModel = responseModel.getResults().get(i);
            films.add(new FilmsModel(idForItem+i,
                    filmModel.getTitle(),
                    filmModel.getOverview(),
                    filmModel.getPosterPath(),
                    filmModel.getVoteCount(),
                    filmModel.getPopularity(),
                    filmModel.getAdult(),
                    filmModel.getReleaseDate()));
        }
        return films;
    }
}
