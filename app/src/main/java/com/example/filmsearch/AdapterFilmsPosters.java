package com.example.filmsearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.filmsearch.model.FilmsModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterFilmsPosters extends RecyclerView.Adapter<AdapterFilmsPosters.ViewHolder> {

    List<FilmsModel> filmsModels;

    private OnFilmClickListener onFilmClickListener;

    Context context;

    public AdapterFilmsPosters(List<FilmsModel> filmsModels, Context context, OnFilmClickListener onFilmClickListener ) {
        this.filmsModels = filmsModels;
        this.context = context;
        this.onFilmClickListener = onFilmClickListener;
    }


    public void addToListAndRefresh(List<FilmsModel> newListFilms) {
        filmsModels.addAll(newListFilms);
        notifyDataSetChanged();
    }

    interface OnFilmClickListener {
          void onFilmClick(FilmsModel filmsModel);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*Привязываем вьюху к лайауту и создаём вьювХолдер*/
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        /*Устанавливаем значения для элементов*/
        holder.bind(position, onFilmClickListener);
//        holder.titleFilm.setText("");
    }

    @Override
    public int getItemCount() {
        return filmsModels.size();
    }

    /*Сам вьювХолдер*/
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleFilm)
        TextView titleFilm;
        @BindView(R.id.posterFilm)
        ImageView posterFilm;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final int position,final OnFilmClickListener onFilmClickListener) {
            titleFilm.setText(filmsModels.get(position).getNameFilm());
            Glide.with(context).load(filmsModels.get(position).getPhotoUrl()).into(posterFilm);
            posterFilm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   onFilmClickListener.onFilmClick(filmsModels.get(position));
                }
            });
        }
    }
}
