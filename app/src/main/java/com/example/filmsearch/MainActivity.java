package com.example.filmsearch;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.filmsearch.databases.FilmsDataBase;
import com.example.filmsearch.model.FilmsModel;
import com.example.filmsearch.model.ResponseModel;
import com.example.filmsearch.utils.ApiService;
import com.example.filmsearch.utils.Converter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements AdapterFilmsPosters.OnFilmClickListener {
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_DESCR = "description";
    public static final String EXTRA_PICTURE = "picture";
    public static final String EXTRA_YEAR = "year";
    public static final String EXTRA_POPULARITY = "popularity";
    public static final String EXTRA_ADULT = "isAdult";

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;



    GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, 2);

    List<FilmsModel> filmsModels = new ArrayList<>();
    AdapterFilmsPosters adapterFilmsPosters = new AdapterFilmsPosters(filmsModels,
            MainActivity.this, MainActivity.this);
    private int pageNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final FilmsDataBase filmsDataBase = Room
                .databaseBuilder(getApplicationContext(), FilmsDataBase.class, "database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        final int count = filmsDataBase.getFilmsDao().countAll();

        if (count > 1) {
            getDataFromDisk(filmsDataBase);
        } else {
            getDataFromServer(filmsDataBase, String.valueOf(pageNumber), 0);
        }


        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int allChildNow = gridLayoutManager.getChildCount();
                    int visibleItemPosition = gridLayoutManager.findFirstCompletelyVisibleItemPosition();
                    int becomeTo = gridLayoutManager.getItemCount();

                    if ((allChildNow + visibleItemPosition) == becomeTo) {
                        String pageForLoad = String.valueOf(pageNumber++);
                        int countInList = filmsModels.size();
                        nextPageFromServer(pageForLoad, countInList);
                    }
                }
            }
        });

    }

    private void getDataFromServer(final FilmsDataBase filmsDataBase, String pageForLoad, int countInList) {
        Disposable disposable = ApiService.getAllPopularFilms(ApiService.apiKey, pageForLoad)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<ResponseModel>() {
                    @Override
                    public void accept(ResponseModel responseModel) throws Exception {

                        filmsModels = Converter.convertRequest(responseModel, 0);

                        filmsDataBase.getFilmsDao().insertAll(filmsModels);

                        AdapterFilmsPosters adapterFilmsPosters = new AdapterFilmsPosters(filmsModels,
                                MainActivity.this, MainActivity.this);

                        /* Тут мы указываем что у нашего списка будет 2 колонки*/
                        recyclerView.setLayoutManager(gridLayoutManager);

                        recyclerView.setAdapter(adapterFilmsPosters);


                        adapterFilmsPosters.notifyDataSetChanged();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        Toast.makeText(MainActivity.this, "Error with internet", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getDataFromDisk(FilmsDataBase filmsDataBase) {
        Disposable disposable = filmsDataBase.getFilmsDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<FilmsModel>>() {
                    @Override
                    public void accept(List<FilmsModel> obsfilmsModels) throws Exception {

                        filmsModels = obsfilmsModels;

                        adapterFilmsPosters = new AdapterFilmsPosters(filmsModels,
                                MainActivity.this, MainActivity.this);

                        recyclerView.setLayoutManager(gridLayoutManager);

                        recyclerView.setAdapter(adapterFilmsPosters);

                        adapterFilmsPosters.notifyDataSetChanged();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        throwable.printStackTrace();
                        Toast.makeText(MainActivity.this, "Error with DataBase", Toast.LENGTH_LONG).show();
                    }
                });
    }


    private void nextPageFromServer(String pageForLoad, final int countToStart) {
        Disposable disposable2 = ApiService.getAllPopularFilms(ApiService.apiKey, pageForLoad)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<ResponseModel>() {
                    @Override
                    public void accept(ResponseModel responseModel) throws Exception {
                        List<FilmsModel> newListFilms = Converter.convertRequest(responseModel, countToStart);

                        filmsModels.addAll(newListFilms);
                        adapterFilmsPosters.addToListAndRefresh(newListFilms);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "Error on next", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void onFilmClick(FilmsModel filmsModel) {
        Intent intent = new Intent(this, FilmActivity.class);
        intent.putExtra(EXTRA_TITLE, filmsModel.getNameFilm());
        intent.putExtra(EXTRA_DESCR, filmsModel.getDescriptionFilm());
        intent.putExtra(EXTRA_POPULARITY, filmsModel.getPopularity());
        intent.putExtra(EXTRA_PICTURE, filmsModel.getPhotoUrl());
        intent.putExtra(EXTRA_YEAR, filmsModel.getReleaseDate());
        intent.putExtra(EXTRA_ADULT, filmsModel.isAdult());
        startActivity(intent);
    }
}
