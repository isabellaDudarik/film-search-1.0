package com.example.filmsearch.databases;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.filmsearch.model.FilmsModel;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class FilmDataBaseDao {
    @Insert
    public abstract void insertAll(List<FilmsModel> filmsModels);

    @Query("SELECT * FROM FilmsModel")
    // позволяет использовать буферизацию. Когда выдираем большое кол-во данных, можно выдирать только по чуть-чуть к примеру обьектов
    public abstract Flowable<List<FilmsModel>> selectAll();                     // тот же обзёрв


    @Query("SELECT COUNT(*) FROM FilmsModel")
    // позволяет использовать буферизацию. Когда выдираем большое кол-во данных, можно выдирать только по чуть-чуть к примеру обьектов
    public abstract Integer countAll();

    @Query("DELETE FROM FilmsModel")
    public abstract void removeAll();

}
