package com.example.filmsearch.databases;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.filmsearch.model.FilmsModel;

@Database(entities = {FilmsModel.class}, version = 2)
public abstract class FilmsDataBase extends RoomDatabase {

    public abstract FilmDataBaseDao getFilmsDao();

}
