package com.example.filmsearch.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class FilmsModel {
    @PrimaryKey
    int id;

    String nameFilm;
    String descriptionFilm;
    String photoUrl;
    int vouteCount;
    double popularity;
    boolean isAdult;
    String releaseDate;

    public FilmsModel(int id,
                      String nameFilm,
                      String descriptionFilm,
                      String photoUrl,
                      int vouteCount,
                      double popularity,
                      boolean isAdult,
                      String releaseDate) {
        this.id = id;
        this.nameFilm = nameFilm;
        this.descriptionFilm = descriptionFilm;
        this.photoUrl = photoUrl;
        this.vouteCount = vouteCount;
        this.popularity = popularity;
        this.isAdult = isAdult;
        this.releaseDate = releaseDate;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameFilm() {
        return nameFilm;
    }

    public void setNameFilm(String nameFilm) {
        this.nameFilm = nameFilm;
    }

    public String getDescriptionFilm() {
        return descriptionFilm;
    }

    public void setDescriptionFilm(String descriptionFilm) {
        this.descriptionFilm = descriptionFilm;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getVouteCount() {
        return vouteCount;
    }

    public void setVouteCount(int vouteCount) {
        this.vouteCount = vouteCount;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public boolean isAdult() {
        return isAdult;
    }

    public void setAdult(boolean adult) {
        isAdult = adult;
    }
}
